package com.valneygama.sala.model

/**
 * Created by valney.gama on 20/02/2018.
 */
class Status {
    var porta : String? = null
    var temperatura : Double? = null
    var umidade : Double? = null

    constructor() {}

    constructor(porta: String?, temperatura: Double?, umidade: Double?) {
        this.porta = porta
        this.temperatura = temperatura
        this.umidade = umidade
    }
}