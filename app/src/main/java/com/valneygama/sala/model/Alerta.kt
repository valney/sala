package com.valneygama.sala.model

/**
 * Created by valney.gama on 20/02/2018.
 */
class Alerta {
    var mensagem: String? = null
    var tipo: String? = null

    constructor(){}
    constructor(mensagem:String?, tipo:String?){
        this.mensagem = mensagem
        this.tipo = tipo
    }
}