package com.valneygama.sala

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.google.firebase.database.*
import com.valneygama.sala.model.Alerta
import com.valneygama.sala.model.Status
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    val database = FirebaseDatabase.getInstance()
    val TAG = "Main"
    var alertas = mutableListOf<Alerta>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val statusReference = database.getReference("status")
        val alertasReference = database.getReference("alertas")

        val statusListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot?) {
                if (dataSnapshot!!.exists()) {
                    val status = dataSnapshot.getValue(Status::class.java)
                    textView1.text = "Temperatura: "+status!!.temperatura+"\n"+
                            "Umidade: "+status!!.umidade+"\n"+
                            "Porta: "+status!!.porta+"\n"
                }
            }

            override fun onCancelled(error: DatabaseError?) {
                error?.let {
                    Log.e(TAG, "Erro ao capturar: " + it.toString())
                }

            }
        }
        val alertasListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot?) {
                if (dataSnapshot!!.exists()) {
                    if (dataSnapshot.childrenCount>0) {
                        // ok
                        alertas.clear()
                        dataSnapshot.children.forEach {
                            val alerta = it.getValue(Alerta::class.java)
                            alertas.add(alerta!!)
                        }
                        val mensagem = "Existem "+alertas.count()+" alertas ativos."
                        //Log.e(TAG,mensagem)
                        //Toast.makeText(this@MainActivity, mensagem, Toast.LENGTH_SHORT)
                    } else {
                        val mensagem = "Nao existe alerta ativo."
                        //Log.e(TAG,mensagem)
                        //Toast.makeText(this@MainActivity, mensagem, Toast.LENGTH_SHORT)
                    }
                }
            }

            override fun onCancelled(error: DatabaseError?) {
                error?.let {
                    Log.e(TAG, "Erro ao capturar: " + it.toString())
                }

            }
        }

        statusReference.addValueEventListener(statusListener)
        alertasReference.addValueEventListener(alertasListener)
    }
}
